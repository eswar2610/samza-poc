./scripts/grid bootstrap
 


1) download gradle and required dependencied
./gradlew installGrid

2) to build samza job package
./gradlew distTar

note: package will be available in build/distributions dir

3)deploy samza project to grid:
  
 ./gradlew deploy

4) start the grid (starts up yarn/kafka/zookeeper):

./gradlew startGrid

if you get error in task ./gradlew runSessionWindow 
Please run the below command
sudo deploy/samza/bin/run-app.sh --config-factory=org.apache.samza.config.factories.PropertiesConfigFactory 
--config-path=file://$PWD/deploy/samza/config/yarn-session-window-example.properties


5) ./gradlew runSessionWindow

6) view all the current Kafka topics:
   
   	./gradlew listKafkaTopics

7) stop all the components:
   
   	./gradlew stopGrid